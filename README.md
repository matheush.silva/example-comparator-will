# Dynamic comparator example

Importe a aplicação como um projeto maven.
Inicie a aplicação a partir da Classe `com.example.comparator.will.ExampleComparatorWillApplication`.

Ela vai subir no endereço: `http://localhost:8080/player/order`.
Na pasta `requests-postman` tem uma coleção do POSTMAN, só importar ela e testar.

As packages Controller e Model são só de apoio. O lance mesmo está no Service.
Tem dois métodos, um deles só faz a construção da response o outro, que ele sim é o bixão, cria um TreeSet (com um Comparator Boladão.)

