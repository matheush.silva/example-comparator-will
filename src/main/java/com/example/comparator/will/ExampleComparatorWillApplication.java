package com.example.comparator.will;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleComparatorWillApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExampleComparatorWillApplication.class, args);
	}

}
