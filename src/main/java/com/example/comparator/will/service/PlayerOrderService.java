package com.example.comparator.will.service;

import com.example.comparator.will.model.OrderTypeEnum;
import com.example.comparator.will.model.Player;
import com.example.comparator.will.model.PlayerOrderRequestModel;
import com.example.comparator.will.model.PlayerResponseModel;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

@Service
public class PlayerOrderService {
    public PlayerResponseModel orderPlayers(PlayerOrderRequestModel playerOrderRequestModel) {
        Set<Player> orderedSet = this.createOrderedSet(playerOrderRequestModel.getOrderType());
        //Add Geral já ordenado.
        orderedSet.addAll(playerOrderRequestModel.getPlayerList());
        return new PlayerResponseModel(orderedSet);
    }

    private Set<Player> createOrderedSet(OrderTypeEnum orderType) {
        Comparator<Player> comparator = null;
        switch (orderType) {
            case BY_AGE:
                // podia ser em Java 8 (player, otherPlayer) ->  player.getAge() - otherPlayer.getAge();
                /* ou um new Comparator<Player>() {
                    @Override
                    public int compare(Player player, Player otherPlayer) {
                        return player.getAge() - otherPlayer.getAge();
                    }
                }  Em Java 7*/
                // Não se apega no java 8, isso é algo que já existia antes.
                comparator = Comparator.comparingInt(Player::getAge);
                break;
            case BY_NAME:
                comparator = Comparator.comparing(Player::getName);
                break;
            case BY_NUMBER:
                comparator = Comparator.comparing(Player::getNumber);
                break;
            default:
                throw new RuntimeException("Invalid order type.");
        }
        // Aqui a mágica acontece... vc tá passando o comparator como construtor do Set,
        // a partir de agora, todos os ADD que der nele, estarão ordenados.
        return new TreeSet<>(comparator);
    }
}
