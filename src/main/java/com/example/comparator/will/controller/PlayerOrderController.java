package com.example.comparator.will.controller;

import com.example.comparator.will.model.PlayerOrderRequestModel;
import com.example.comparator.will.model.PlayerResponseModel;
import com.example.comparator.will.service.PlayerOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/player")
public class PlayerOrderController {

    @Autowired
    private PlayerOrderService service;

    @PostMapping("/order")
    public ResponseEntity<PlayerResponseModel> orderPlayers(@RequestBody PlayerOrderRequestModel playerOrderRequestModel) {
        return ResponseEntity.ok(service.orderPlayers(playerOrderRequestModel));
    }
}
