package com.example.comparator.will.model;

import java.util.List;

public class PlayerOrderRequestModel {

    private OrderTypeEnum orderType;

    private List<Player> playerList;

    public OrderTypeEnum getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderTypeEnum orderType) {
        this.orderType = orderType;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }
}