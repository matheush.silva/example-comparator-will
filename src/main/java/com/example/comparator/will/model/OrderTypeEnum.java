package com.example.comparator.will.model;

public enum OrderTypeEnum {
    BY_AGE, BY_NAME, BY_NUMBER
}
