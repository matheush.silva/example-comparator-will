package com.example.comparator.will.model;

import java.util.Set;

public class PlayerResponseModel {

    private Set<Player> orderedPlayerList;

    public PlayerResponseModel() {

    }

    public PlayerResponseModel(Set<Player> orderedPlayerList) {
        this.orderedPlayerList = orderedPlayerList;
    }

    public Set<Player> getOrderedPlayerList() {
        return orderedPlayerList;
    }

    public void setOrderedPlayerList(Set<Player> orderedPlayerList) {
        this.orderedPlayerList = orderedPlayerList;
    }
}